package com.ali.test.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

/**
 * Created by Ali Sabir on 5/30/2016.
 */
public class Utils {

    public static Bitmap createImageFromMask(Context context, int maskImage, int originalImage) {
        Bitmap mask=null;
        if(mask==null) {
            mask= BitmapFactory.decodeResource(context.getResources(), maskImage);
        }
        Bitmap original=null;
        if(original==null) {
            original = BitmapFactory.decodeResource(context.getResources(), originalImage);
        }
        original = Bitmap.createScaledBitmap(original, mask.getWidth(), mask.getHeight(), true);
        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);
        return result;
    }

}
