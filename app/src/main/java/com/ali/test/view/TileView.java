package com.ali.test.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ali.test.R;
import com.ali.test.constents.Constents;
import com.ali.test.utils.Utils;

/**
 * Created by Ali Sabir on 5/30/2016.
 */


    public class TileView extends LinearLayout {

        View view;
    private ImageView imgItem;
    private TextView txtCounter;


    public TileView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view=  mInflater.inflate(R.layout.tile_view, this, true);
        init(attrs);
    }



    public TileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view=   mInflater.inflate(R.layout.tile_view, this, true);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view=  mInflater.inflate(R.layout.tile_view, this, true);
        init(attrs);
    }


        public TileView( Context context) {
            super(context);
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         this.view=   mInflater.inflate(R.layout.tile_view, this, true);
            init(null);
        }


    private void init(AttributeSet attrs) {

      this.imgItem= (ImageView) view.findViewById(R.id.imgGellaryItem);
       this.txtCounter= (TextView) view.findViewById(R.id.txtCounter);

    if(attrs!=null) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TileView);
        if(a.hasValue(R.styleable.TileView_alignCounterText))
        {
            int textPosition = a.getInt(R.styleable.TileView_alignCounterText, 2);
            switch (textPosition)
            {
                case 1:alignTopLeft();break;
                case 2:alignTopRight();break;
                case 3:alignBottomLeft();break;
                case 4:alignBottomRight();break;
            }
        }



    }


    }

    private void alignTopLeft() {

        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)txtCounter.getLayoutParams();
        params.gravity = Gravity.TOP|Gravity.LEFT;
        txtCounter.setLayoutParams(params);


    }

    private void alignTopRight()
    {
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)txtCounter.getLayoutParams();
        params.gravity = Gravity.TOP|Gravity.RIGHT;
        txtCounter.setLayoutParams(params);

    }

    private void alignBottomLeft()
    {
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)txtCounter.getLayoutParams();
        params.gravity = Gravity.BOTTOM|Gravity.LEFT;
        txtCounter.setLayoutParams(params);

    }

    private void alignBottomRight()
    {
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)txtCounter.getLayoutParams();
        params.gravity = Gravity.BOTTOM|Gravity.RIGHT;
        txtCounter.setLayoutParams(params);
    }

    public void setCounterText(int count11) {

        this.txtCounter.setText(count11+1+"");

    }

    public void setImage(int image11) {
        imgItem.setImageBitmap(Utils.createImageFromMask(getContext(),R.drawable.image_frame_arc_mask,image11));

    }
}


