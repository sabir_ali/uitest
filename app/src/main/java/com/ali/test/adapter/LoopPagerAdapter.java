package com.ali.test.adapter;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ali.test.R;
import com.ali.test.constents.Constents;
import com.ali.test.object.PageItems;
import com.ali.test.utils.Utils;
import com.ali.test.view.TileView;

import java.util.ArrayList;

/**
 * Created by Ali SAbir on 5/31/2016.
 */
public class LoopPagerAdapter extends PagerAdapter {
    private final ArrayList<PageItems> pageContent;
    private final Context context;
    private Handler handler;

    public int getCount() {
        return pageContent.size();
    }

    public  LoopPagerAdapter(Context context,ArrayList<PageItems> pageCounts)
    {
        this.pageContent =pageCounts;
        this.context=context;
        this.handler=new Handler();

    }
    public Object instantiateItem(ViewGroup container, final int position) {

        Log.d(Constents.DEBUG_KEY,"Object index initiated "+position);
        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int resId = R.layout.gelary_page_layout;;
        final View view = inflater.inflate(resId, container, false);

        handler.post(new Runnable() {
            public void run() {
                PageItems pageItems= pageContent.get(position);

                TileView tileView11 = (TileView) view.findViewById(R.id.tileView11);
                TileView tileView12 = (TileView) view.findViewById(R.id.tileView12);
                TileView tileView21 = (TileView) view.findViewById(R.id.tileView21);
                TileView tileView22 = (TileView) view.findViewById(R.id.tileView22);

                tileView11.setCounterText(pageItems.getCount11());
                tileView11.setImage(pageItems.getImage11());

                tileView12.setCounterText(pageItems.getCount12());
                tileView12.setImage(pageItems.getImage12());

                tileView21.setCounterText(pageItems.getCount21());
                tileView21.setImage(pageItems.getImage21());

                tileView22.setCounterText(pageItems.getCount22());
                tileView22.setImage(pageItems.getImage22());
            }
        });





        ((ViewPager) container).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        Log.d(Constents.DEBUG_KEY,"View page item destroid "+position);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
