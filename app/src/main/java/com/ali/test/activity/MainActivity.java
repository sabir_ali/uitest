/* Copyright (C) 2012 The Android Open Source Project

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.ali.test.activity;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ali.test.R;
import com.ali.test.adapter.LoopPagerAdapter;
import com.ali.test.adapter.LoopPagerAdapterWrapper;
import com.ali.test.constents.Constents;
import com.ali.test.object.PageItems;
import com.ali.test.utils.Utils;
import com.ali.test.view.LoopViewPager;

import java.util.ArrayList;


public class MainActivity extends Activity implements  ViewPager.OnPageChangeListener, View.OnClickListener {
    private RadioGroup radioGroup;
    private LoopViewPager loopViewPager;
    private LoopPagerAdapterWrapper pagerAdapterWrapper;
    private ArrayList<PageItems> gellaryItems;
    private boolean ignorePageListener=false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int[] images=new int[12];
        images[0]=R.drawable.image_1;
        images[1]=R.drawable.image_2;
        images[2]=R.drawable.image_3;
        images[3]=R.drawable.image_4;
        images[4]=R.drawable.image_5;
        images[5]=R.drawable.image_6;
        images[6]=R.drawable.image_7;
        images[7]=R.drawable.image_8;
        images[8]=R.drawable.image_9;
        images[9]=R.drawable.image_10;
        images[10]=R.drawable.image_11;
        images[11]=R.drawable.image_12;

        gellaryItems=new ArrayList<>();

        radioGroup= (RadioGroup) findViewById(R.id.radioGroup);
        for (int i=0;i<images.length;)
        {
            PageItems pageIte = new PageItems();


                pageIte.setCount11(i);
                pageIte.setImage11(images[i++]);

                pageIte.setCount12(i);
                pageIte.setImage12(images[i++]);

                pageIte.setCount21(i);
                pageIte.setImage21(images[i++]);

                pageIte.setCount22(i);
                pageIte.setImage22(images[i++]);

            RadioButton radioButton=new RadioButton(MainActivity.this);
            gellaryItems.add(pageIte);
            radioButton.setTag(gellaryItems.size()+"");
            radioButton.setOnClickListener(this);

            radioButton.setButtonDrawable(R.drawable.radio_drawable);
            radioButton.setPadding(20,20,20,20);

            radioGroup.addView(radioButton);
        }



        Log.d(Constents.DEBUG_KEY,"PAges "+gellaryItems.size());

        LoopPagerAdapter pagerAdapter=new LoopPagerAdapter(MainActivity.this,gellaryItems);

         pagerAdapterWrapper=new LoopPagerAdapterWrapper(pagerAdapter);

       loopViewPager  = (LoopViewPager) findViewById(R.id.loopViewPager);
        loopViewPager.setAdapter(pagerAdapterWrapper);
        loopViewPager.addOnPageChangeListener(this);
        loopViewPager.setBoundaryCaching(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
             loopViewPager.setCurrentItem(1,false);
            }
        },500);


    }



    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

if(ignorePageListener) {ignorePageListener=false;return;}
        int pageNo=getPos(gellaryItems.size(),pagerAdapterWrapper.toRealPosition(position));

        Log.d(Constents.DEBUG_KEY,"Page selected "+position+" Real pos "+pageNo);

      RadioButton radioButton= (RadioButton) radioGroup.getChildAt(pageNo);
       if(radioButton!=null) radioButton.setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



    @Override
    public void onClick(View v) {

        if(v instanceof RadioButton) {
            ignorePageListener=true;
            String butNo = (String) v.getTag();
            int pageIndex = Integer.parseInt(butNo);
            int requestedPage=pageIndex+1;
            requestedPage=getPos(gellaryItems.size(),requestedPage);
            Log.d(Constents.DEBUG_KEY,"Request to scroll "+requestedPage);
            loopViewPager.setCurrentItem(requestedPage, true);
        }
    }

    private int getPos(int size,int posi)
    {
        Log.d(Constents.DEBUG_KEY,"Page size "+size+" position "+posi);

        int index=posi-1;
        if(index>=0) return index;
        else  return size+posi-1;
    }
}

