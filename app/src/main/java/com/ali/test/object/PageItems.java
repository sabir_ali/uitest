package com.ali.test.object;

import java.util.ArrayList;

/**
 * Created by Ali Sabir on 5/31/2016.
 */
public class PageItems {

int[] drawables=new int[4];
    int[] count=new int[4];

    public void setImage11(int resId)
    {
        this.drawables[0]=resId;
    }
    public void setCount11(int count)
    {
        this.count[0]=count;
    }

    public void setImage12(int resId)
    {
        this.drawables[1]=resId;
    }
    public void setCount12(int count)
    {
        this.count[1]=count;
    }

    public void setImage21(int resId)
    {
        this.drawables[2]=resId;
    }
    public void setCount21(int count)
    {
        this.count[2]=count;
    }

    public void setImage22(int resId)
    {
        this.drawables[3]=resId;
    }
    public void setCount22(int count)
    {
        this.count[3]=count;
    }

    public  int getImage11()
    {
        return drawables[0];
    }
    public int getCount11()
    {
        return count[0];
    }

    public int getImage12()
    {
        return drawables[1];
    }
    public int getCount12()
    {
        return count[1];
    }

    public int getImage21()
    {
        return drawables[2];
    }
    public int getCount21()
    {
        return count[2];
    }

    public int getImage22()
    {
        return drawables[3];
    }
    public int getCount22()
    {
        return count[3];
    }
}
